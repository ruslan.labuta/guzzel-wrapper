<?php

namespace EightPoints\Bundle\GuzzleBundle\DependencyInjection;

use       Symfony\Component\Config\FileLocator,
          Symfony\Component\DependencyInjection\ContainerBuilder,
          Symfony\Component\DependencyInjection\Loader\XmlFileLoader,
          Symfony\Component\DependencyInjection\Loader\YamlFileLoader,
          Symfony\Component\HttpKernel\DependencyInjection\Extension,
          Symfony\Component\Config\Definition\Processor,
          Symfony\Component\DependencyInjection\Definition;

/**
 * GuzzleExtension
 *
 * @package   EightPoints\Bundle\GuzzleBundle\DependencyInjection
 */
class GuzzleExtension extends Extension
{
    /**
     * Loads the Guzzle configuration.
     * @param   array            $configs   an array of configuration settings
     * @param   ContainerBuilder $container a ContainerBuilder instance
     *
     * @throws  \InvalidArgumentException
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configPath = implode(DIRECTORY_SEPARATOR, array(__DIR__, '..', 'Resources', 'config'));
        $loader     = new YamlFileLoader($container, new FileLocator($configPath));

        $loader->load('services.yml');

        $processor     = new Processor();
        $configuration = new Configuration($this->getAlias(), $container->getParameter('kernel.debug'));
        $config        = $processor->processConfiguration($configuration, $configs);

        foreach ($config['clients'] as $client => $clientData) {
            // set base url
            $guzzleClient = new Definition('%guzzle.http_client.class%');
            $guzzleClient->addArgument($clientData['base_url']);

            // set service name based on client name
            $serviceName = sprintf('%s.client.%s', $this->getAlias(), $client);
            $container->setDefinition($serviceName, $guzzleClient);

            // set headers
            if (isset($clientData['headers'])) {
                $this->addHeaders($clientData['headers'], $container, $serviceName);
            }

            // set post data
            if (isset($clientData['post_data']) &&
                !empty($clientData['post_data'])) {
                $this->addPostData($clientData['post_data'], $container, $serviceName);
            }
        }
    }

    /**
     * Set up HTTP headers
     * @param array $headers
     * @param ContainerBuilder $container
     */
    protected function addHeaders(array $headers, ContainerBuilder $container, $serviceName)
    {
        $container->setParameter('guzzle.plugin.header.headers', $headers);
        $container->getDefinition($serviceName)
                  ->addMethodCall('getEventDispatcher')
                  ->addMethodCall('addSubscriber', array($container->getDefinition('guzzle.plugin.header')));
    }

    /**
     * Add post data
     * @param array $options
     * @param ContainerBuilder $container
     * @param $serviceName
     */
    protected function addPostData(array $options, ContainerBuilder $container, $serviceName)
    {
        $container->setParameter('guzzle.plugin.post.servicename', $serviceName);
        $container->setParameter('guzzle.plugin.post.postdata', $options);

        $container->getDefinition($serviceName)
                  ->addMethodCall('getEventDispatcher')
                  ->addMethodCall('addSubscriber', array($container->getDefinition('guzzle.plugin.post')));
    }


    /**
     * Returns alias of class
     * @return  string
     */
    public function getAlias()
    {
        return 'guzzle';
    }
}
