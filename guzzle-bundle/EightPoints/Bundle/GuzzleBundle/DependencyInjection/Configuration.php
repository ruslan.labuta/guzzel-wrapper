<?php

namespace EightPoints\Bundle\GuzzleBundle\DependencyInjection;

use       Symfony\Component\Config\Definition\Builder\TreeBuilder,
          Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * Configuration
 *
 * @package   EightPoints\Bundle\GuzzleBundle\DependencyInjection
 */
class Configuration implements ConfigurationInterface
{

    /**
     * @var string $alias
     */
    protected $alias;

    /**
     * @var boolean $debug
     */
    protected $debug;

    /**
     * @param   string  $alias
     * @param   boolean $debug
     */
    public function __construct($alias, $debug = false)
    {
        $this->alias = $alias;
        $this->debug = (boolean) $debug;
    }

    /**
     * Generates the configuration tree builder
     * @return TreeBuilder
     */
    public function getConfigTreeBuilder()
    {
        $builder = new TreeBuilder();
        $builder->root($this->alias)
                ->children()
                       ->arrayNode('clients')
                       ->useAttributeAsKey('name')
                       ->prototype('array')
                              ->children()
                                     //set base url
                                    ->scalarNode('base_url')->defaultValue(null)->end()
                                     // set post data
                                    ->arrayNode('post_data')
                                         ->prototype('scalar')->end()
                                    ->end()
                              ->end()
                      // ->booleanNode('logging')->defaultValue($this->debug)->end()
                       ->end()
                ->end();
        return $builder;
    }

}