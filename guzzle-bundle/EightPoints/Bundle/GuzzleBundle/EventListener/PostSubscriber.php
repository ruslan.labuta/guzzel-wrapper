<?php
/**
 * Created by PhpStorm.
 * User: labuta
 * Date: 5/12/16
 * Time: 1:19 PM
 */

namespace EightPoints\Bundle\GuzzleBundle\EventListener;


use       Guzzle\Common\Event,
          Symfony\Component\DependencyInjection\ContainerBuilder,
          Symfony\Component\EventDispatcher\EventSubscriberInterface;


class PostSubscriber implements EventSubscriberInterface
{
    public $postData;
    protected $serviceName;

    public function __construct($serviceName, array $postData)
    {
        $this->setServiceName($serviceName);
        $this->setPostData($postData);
    }

    /**
     * Init $postData
     * @param array $postData
     */
    public function setPostData(array $postData)
    {
        $this->postData[$this->getServiceName()] = $postData;
    }

    /**
     * Set service client name
     * @param $serviceName
     */
    public function setServiceName($serviceName)
    {
        $this->serviceName = $serviceName;
    }


    /**
     * Function return client service name
     * @return mixed
     */
    public function getServiceName()
    {
        return $this->serviceName;
    }


    /**
     * Subscribe on event
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return array('client.create_request' => 'onCreateRequest');
    }

    /**
     * Function set post data
     * @param Event $event
     */
    public function onCreateRequest(Event $event)
    {
        $request = $event['request'];
        $request->addPostFields($this->postData[$this->getServiceName()]);
    }
}
